/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.scoreboard;

import me.boomboompower.deathwalls.utils.MessageUtils;
import me.boomboompower.deathwalls.utils.RankUtils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.util.Date;

public class PlayerTeam {

    private Player player;
    private ScoreboardManager manager = Bukkit.getScoreboardManager();
    private Scoreboard board = manager.getNewScoreboard();

    public PlayerTeam(Player player, int timer) {
        this.player = player;
        Date d = new Date();
        String format = d.getDay() + "/" + d.getMonth() + "/" + d.getYear();
        Team t = board.registerNewTeam(player.getName());
        t.addEntry(player.getName());
        t.setPrefix(getPrefix());
        t.setDisplayName(getPrefix());
        t.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);

        Objective o = board.registerNewObjective("info", "dummy");
        o.setDisplaySlot(DisplaySlot.SIDEBAR);
        o.setDisplayName(MessageUtils.translate("&c&lGame Information"));

        Score s = o.getScore(MessageUtils.translate("&7&lTime: &c&l" + format));
        s.setScore(10);

        Score empty = o.getScore("");
        empty.setScore(9);

        Score tim = o.getScore(MessageUtils.translate("&9&lTime left: &c&l" + timer));
        tim.setScore(8);

        Objective h = board.registerNewObjective("health", "health");
        h.setDisplaySlot(DisplaySlot.BELOW_NAME);
        h.setDisplayName(MessageUtils.translate(" &c&l❤"));
        player.setHealth(player.getHealth()); //Update their health

        player.setScoreboard(board);
    }

    public String getPrefix() {
        return RankUtils.getPrefix(RankUtils.getRank(player));
    }
}
