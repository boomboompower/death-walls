/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.utils;

import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.util.Set;

public class ScoreboardEditor {

    private Scoreboard scoreboard;

    public ScoreboardEditor(Team team) {
        this(team.getScoreboard());
    }

    public ScoreboardEditor(Objective objective) {
        this(objective.getScoreboard());
    }

    public ScoreboardEditor(Scoreboard scoreboard) {
        if (scoreboard == null) throw new IllegalStateException("Scoreboard cannot be null!");
        this.scoreboard = scoreboard;
    }

    public void clearSlot(DisplaySlot slot) {
        scoreboard.clearSlot(slot);
    }

    @Deprecated // Can contain things other than players
    public Set<Score> getScore(Player player) {
        return getScore(player.getName());
    }

    public Set<Score> getScore(String entityName) {
        return scoreboard.getScores(entityName);
    }

    @Deprecated // Can contain things other than players
    public void resetScore(Player player) {
        resetScore(player.getName());
    }

    public void resetScore(String entityName) {
        scoreboard.resetScores(entityName);
    }

    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    public class ScoreboardObjective {
        public void createObjective(String name, String criteria) {
            scoreboard.registerNewObjective(name, criteria);
        }

        public void setObjectiveDisplayName(String objective, String name) {
            scoreboard.getObjective(objective).setDisplayName(MessageUtils.translate(name));
        }

        public void setObjectiveDisplaySlot(String objective, DisplaySlot slot) {
            scoreboard.getObjective(objective).setDisplaySlot(slot);
        }

        public void removeObjective(String name) {
            scoreboard.getObjective(name).unregister();
        }
    }

    public class ScoreboardTeam {
        public void createTeam(String teamName) {
            scoreboard.registerNewTeam(teamName);
        }

        @Deprecated // Can contain things other than players
        public void getPlayerTeam(Player player) {
            getTeam(player.getName());
        }

        public void getTeam(String entityName) {
            scoreboard.getEntryTeam(entityName);
        }

        @Deprecated // Can contain things other than players
        public void addToTeam(String teamName, Player p) {
            addToTeam(teamName, p.getName());
        }

        public void addToTeam(String teamName, String entityName) {
            scoreboard.getTeam(teamName).addEntry(entityName);
        }

        public void setPrefix(String teamName, String prefix) {
            scoreboard.getTeam(teamName).setPrefix(prefix);
        }

        public void setSuffix(String teamName, String suffix) {
            scoreboard.getTeam(teamName).setSuffix(MessageUtils.translate(suffix));
        }

        public void removeTeam(String teamName) {
            scoreboard.getTeam(teamName).unregister();
        }
    }
}
