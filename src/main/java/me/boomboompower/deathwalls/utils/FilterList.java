/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.utils;

import java.util.ArrayList;
import java.util.List;

public abstract class FilterList {

    /*
     * I hate making these filters, but someone has to do it.
     */

    private static List<String> filterList = new ArrayList<String>();

    private FilterList() {}

    static {
        addToFilter("cunt");
        addToFilter("fag");
        addToFilter("faggot");
        addToFilter("faggit");
        addToFilter("fagg1t");
        addToFilter("fucker");
        addToFilter("fuker");
        addToFilter("fuqer");
        addToFilter("fuck");
        addToFilter("fuq");
        addToFilter("fak");
        addToFilter("dick");
        addToFilter("d!ck");
        addToFilter("d1ck");
        addToFilter("tits");
        addToFilter("t1ts");
        addToFilter("dik");
        addToFilter("dic");
        addToFilter("diq");
        addToFilter("pussy");
        addToFilter("pusy");
        addToFilter("pusie");
        addToFilter("pussie");
        addToFilter("pu$$ie");
        addToFilter("pu$$y");
        addToFilter("bitch");
        addToFilter("b1tch");
        addToFilter("boner");
        addToFilter("b0ner");
        addToFilter("nigger");
        addToFilter("n1gger");
        addToFilter("asshole");
        addToFilter("a$$hole");
        addToFilter("a55hole");
        addToFilter("arsehole");
        addToFilter("ar$ehole");
        addToFilter("ar5shole");
        addToFilter("assmunch");
        addToFilter("a$$munch");
        addToFilter("a55munch");
        addToFilter("assmuncher");
        addToFilter("a$$muncher");
        addToFilter("a55muncher");
        addToFilter(" ez"); // Good sportsmanship
    }

    public static void addToFilter(String word) {
        filterList.add(word);
    }

    public static boolean hasWords(String message) {
        boolean x = false;
        for (int i = 0; i < getFilterList().size(); i++) {
            if (message.contains(getFilterList().get(i))) x = true;
        }
        return x;
    }

    public static String removeWords(String message) {
        for (int i = 0; i < getFilterList().size(); i++) {
            message.replace(getFilterList().get(i), "");
        }
        return message;
    }

    public static List<String> getFilterList() {
        return filterList;
    }
}
