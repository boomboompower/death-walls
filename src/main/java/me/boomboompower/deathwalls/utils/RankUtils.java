/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.utils;

import org.bukkit.entity.Player;

import java.util.HashMap;

import static org.bukkit.Bukkit.*;

public class RankUtils {

    private static HashMap<Player, Rank> ranks = new HashMap<Player, Rank>();

    static {
        ranks.put(getPlayer("boomboompower"), Rank.ADMINISTRATOR);
        ranks.put(getPlayer("PixelPorridge"), Rank.HELPER);
        ranks.put(getPlayer("Technoblade"), Rank.YOUTUBE);
        ranks.put(getPlayer("Huahwi"), Rank.YOUTUBE);
    }

    public RankUtils(Player player, Rank rank) {
        ranks.put(player, rank);
    }

    public static void setRank(Player player, Rank rank) {
        if (ranks.containsKey(player)) ranks.remove(player);
        ranks.put(player, rank);
    }

    public static Rank getRank(Player player) {
        return ranks.get(player) != null ? ranks.get(player) : Rank.DEFAULT;
    }

    /**
     * Gets the string from {@link #getPrefix(Rank)}
     *
     * @param rank The rank to get the color from
     * @return The color of the rank/prefix
     */
    public static String getColor(Rank rank) {
        return getPrefix(rank).split(String.valueOf(MessageUtils.getColorChar()))[0];
    }

    /**
     * Using double & to make the getColor method work
     *
     * @param rank The rank to get the prefix from
     * @return The prefix for the rank
     */
    public static String getPrefix(Rank rank) {
        String prefix;
        switch (rank) {
            case VIP:
                prefix = MessageUtils.translate("&a&a[VIP] ");
                break;
            case MVP:
                prefix = MessageUtils.translate("&b&b[MVP] ");
                break;
            case YOUTUBE:
                prefix = MessageUtils.translate("&5&5[YT] ");
                break;
            case HELPER:
                prefix = MessageUtils.translate("&9&9[HELPER] ");
                break;
            case ADMINISTRATOR:
                prefix = MessageUtils.translate("&c&c[ADMIN] ");
                break;
            default:
                prefix = "&7&7";
                break;
        }
        return prefix;
    }

    public enum Rank {
        DEFAULT,
        VIP,
        MVP,
        YOUTUBE,
        HELPER,
        ADMINISTRATOR;

        Rank() {}
    }
}
