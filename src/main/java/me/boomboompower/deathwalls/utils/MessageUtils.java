/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class MessageUtils {

    private MessageUtils() {}

    public static void sendToPlayer(Player p, String message) {
        p.sendMessage(translate(message));
    }

    public static void sendToConsole(String message) {
        Bukkit.getConsoleSender().sendMessage(translate(message));
    }

    public static void duelSend(CommandSender sender, String message) {
        if (sender instanceof Player) {
            sendToPlayer((Player) sender, message);
        } else {
            sendToConsole(message);
        }
    }

    public static String translate(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static char getColorChar() {
        return '§';
    }
}
