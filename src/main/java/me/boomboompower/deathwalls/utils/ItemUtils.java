/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.utils;

import me.boomboompower.api.builder.ItemBuilder;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public abstract class ItemUtils {

    private ItemUtils() {}

    public static ItemStack getSelector() {
        ItemBuilder builder = new ItemBuilder(Material.COMPASS);
        builder.setDisplayName("&cPlayer Selector");
        builder.setLore("&cRight click to teleport to a random player!");
        builder.setUnbreakable(true);
        return builder.getItemStack();
    }

    public static ItemStack getQuit() {
        ItemBuilder builder = new ItemBuilder(Material.BED);
        builder.setDisplayName("&4Quit");
        builder.setLore("&4Right click to leave!");
        builder.setUnbreakable(true);
        return builder.getItemStack();
    }

    public static ItemStack getShears() {
        ItemBuilder builder = new ItemBuilder(Material.SHEARS);
        builder.setDisplayName("&cTNT Shears");
        builder.setLore("&4Right click TNT to disable it!");
        builder.setUnbreakable(true);
        return builder.getItemStack();
    }

    public static ItemStack getUselessBlazeRod() {
        ItemBuilder builder = new ItemBuilder(Material.BLAZE_ROD);
        builder.setDisplayName("&cUseless rod");
        builder.setLore("&bThis actually useless.");
        return builder.getItemStack();
    }
}
