/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.commands;

import me.boomboompower.deathwalls.Main;
import me.boomboompower.deathwalls.utils.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.spigotmc.SpigotConfig;

public class CommandKick implements CommandExecutor {

    private Main main;
    private String command = "kick";

    public CommandKick(Main main) {
        this.main = main;

        main.getCommand(command).setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase(command)) {
            if (sender instanceof Player && !sender.hasPermission("deathwalls.admin")) {
                MessageUtils.sendToPlayer((Player) sender, SpigotConfig.unknownCommandMessage);
            } else {
                if (args.length < 1) {
                    MessageUtils.duelSend(sender, "&cIncorrect usage. Use &4/kick <player> <message>&c instead!");
                } else {
                    Player p = Bukkit.getPlayer(args[0]);
                    String r = "&cKicked by moderator!";
                    if (args.length >= 2) r = getArguments(args);
                    if (p.isOnline()) {
                        p.kickPlayer(MessageUtils.translate(r));
                        Bukkit.broadcastMessage(format(p, r));
                    } else {
                        MessageUtils.duelSend(sender, "&cThe player &4" + p.getName() + "&c is not online!");
                    }
                }
            }
        }
        return true;
    }

    private String format(Player player, String reason) {
        return "&4%PLAYER% &chas been kicked for&4 %MESSAGE%".replace("%PLAYER%", player.getName()).replace("%MESSAGE%", reason);
    }

    private String getArguments(String[] args) {
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            builder.append(args[i]);
            builder.append(" ");
        }
        return builder.toString().trim();
    }
}
