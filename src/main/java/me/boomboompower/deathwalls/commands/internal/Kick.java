/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.commands.internal;

import me.boomboompower.deathwalls.commands.DeathWallsCommand;
import me.boomboompower.deathwalls.utils.MessageUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class Kick extends DeathWallsCommand {

    protected Kick(String command) {
        super("kick");
        this.description = "Kicks a certain player.";
        this.usageMessage = "/kick <player> [Reason...]";
        this.setPermission("deathwalls.admin");
    }

    @Override
    public boolean execute(CommandSender sender, String cmd, String[] args) {
        if(!this.testPermission(sender)) {
            return true;
        } else if(args.length >= 1 && args[0].length() != 0) {
            Player player = Bukkit.getPlayerExact(args[0]);
            if(player != null) {
                String reason = MessageUtils.translate("&cAn error occured in your connection to the server.");
                if(args.length > 1) {
                    reason = this.createString(args, 1);
                }
                player.kickPlayer(reason);
            } else {
                sender.sendMessage(args[0] + " not found.");
            }

            return true;
        } else {
            sender.sendMessage(ChatColor.DARK_RED + "Usage: " + this.usageMessage);
            return false;
        }
    }

    String createString(String[] args, int start) {
        return this.createString(args, start, " ");
    }

    String createString(String[] args, int start, String glue) {
        StringBuilder string = new StringBuilder();

        for(int x = start; x < args.length; ++x) {
            string.append(args[x]);
            if(x != args.length - 1) {
                string.append(glue);
            }
        }

        return string.toString();
    }

    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        Validate.notNull(sender, "Sender cannot be null");
        Validate.notNull(args, "Arguments cannot be null");
        Validate.notNull(alias, "Alias cannot be null");
        return super.tabComplete(sender, alias, args);
    }
}
