/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.commands.internal;

import me.boomboompower.deathwalls.commands.DeathWallsCommand;
import me.boomboompower.deathwalls.listeners.Chat;
import me.boomboompower.deathwalls.utils.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Nick extends DeathWallsCommand {

    private boolean send = false;

    protected Nick(String command) {
        super("nick");
        this.description = "Change your chat name.";
        this.usageMessage = "/nick <name>";
        this.setPermission("deathwalls.youtube.nick");
    }

    @Override
    public boolean execute(CommandSender sender, String cmd, String[] args) {
        if(!this.testPermission(sender)) {
            return true;
        } else if(args.length >= 0 && args[0].length() != 0) {
            if (sender instanceof Player) {
                for (OfflinePlayer all : Bukkit.getOfflinePlayers()) {
                    if (args[0].equals(all.getName())) {
                        sender.sendMessage(MessageUtils.translate("&cYou cannot use a real players name!"));
                        send = false;
                        break;
                    } else {
                        send = true;
                    }
                } if (send) {
                    Chat.setNick((Player) sender, args[0]);
                    return true;
                }
            }
            return false;
        }
        return false;
    }
}
