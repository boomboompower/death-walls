/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.commands.internal;

import me.boomboompower.deathwalls.commands.DeathWallsCommand;
import me.boomboompower.deathwalls.utils.MessageUtils;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Crash extends DeathWallsCommand {

    protected Crash(String command) {
        super("crash");
        this.description = "Crashes the server disconnecting all players.";
        this.usageMessage = "/crash";
        this.setPermission("deathwalls.admin");
    }

    @Override
    public boolean execute(CommandSender sender, String cmd, String[] args) {
        if(!this.testPermission(sender)) {
            return true;
        } else {
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.kickPlayer(MessageUtils.translate("&cAn error occured in that lobby, so you have been routed to limbo"));
            }
            return true;
        }
    }
}
