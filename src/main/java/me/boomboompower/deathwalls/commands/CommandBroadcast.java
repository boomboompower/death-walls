/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.commands;

import me.boomboompower.deathwalls.Main;
import me.boomboompower.deathwalls.utils.MessageUtils;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.spigotmc.SpigotConfig;

public class CommandBroadcast implements CommandExecutor {

    private Main main;
    private String format = "&7[&4DeathWalls&7] ";
    private String command = "broadcast";

    public CommandBroadcast(Main main) {
        this.main = main;

        main.getCommand(command).setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase(command)) {
            if (sender instanceof Player && !sender.hasPermission("deathwalls.admin")) {
                MessageUtils.sendToPlayer((Player) sender, SpigotConfig.unknownCommandMessage);
            } else {
                if (args.length < 1) {
                    MessageUtils.duelSend(sender, "&cIncorrect usage. Use &4/alert <message>&c instead!");
                } else {
                    Bukkit.broadcastMessage(format + MessageUtils.translate(getArguments(args)));
                }
            }
        }
        return true;
    }

    private String getArguments(String[] args) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            builder.append(args[i]);
            builder.append(" ");
        }
        return builder.toString().trim();
    }
}
