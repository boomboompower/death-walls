/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public abstract class DeathWallsCommand extends Command {

    private ArrayList<String> commands = new ArrayList<String>();

    protected DeathWallsCommand(String command) {
        super(command != null ? command : "none");
    }

    protected DeathWallsCommand(String command, String description, String usageMessage, List<String> aliases) {
        super(command != null ? command : "none", description != null ? description : "none", usageMessage != null ? usageMessage : "none", aliases != null ? aliases : new ArrayList<String>());
    }

    @Override
    public abstract boolean execute(CommandSender sender, String cmd, String[] args);
}
