/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.listeners;

import me.boomboompower.deathwalls.Main;
import me.boomboompower.deathwalls.blocks.DIAMOND_ORE;
import me.boomboompower.deathwalls.blocks.IRON_ORE;
import me.boomboompower.deathwalls.blocks.RED_SAND;
import me.boomboompower.deathwalls.events.GameStateChangeEvent;
import me.boomboompower.deathwalls.events.PlayerSpectatorEvent;
import me.boomboompower.deathwalls.utils.ItemUtils;
import me.boomboompower.deathwalls.utils.MessageUtils;
import me.boomboompower.deathwalls.utils.RankUtils;

import org.bukkit.*;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.ArrayList;

public class Players implements Listener {

    private static ArrayList<Player> players = new ArrayList<Player>();
    private static int timer = 300;
    private Main main;

    public Players(Main main) {
        this.main = main;
        Bukkit.getPluginManager().registerEvents(this, main);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(main, new Runnable() {
            @Override
            public void run() {
                if (timer >= 1) timer--;
                else Main.setGameState(Main.GameState.FINISHED);
            }
        }, 0, 20);
    }

    @EventHandler
    private void onPlayerJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        String f = RankUtils.getPrefix(RankUtils.getRank(p));
        event.setJoinMessage(null);
        if (!isFull()) {
            add(p);
            p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(16D);
            event.setJoinMessage(MessageUtils.translate(f + "&e joined the game! &6(" + Bukkit.getOnlinePlayers().size() + "/4)"));
        } else {
            p.kickPlayer(MessageUtils.translate("&cThis game is full, try again later!"));
        }
    }

    @EventHandler
    private void onPlayerKick(PlayerKickEvent event) {
        event.setLeaveMessage(null);
        event.setReason(MessageUtils.translate(""));
    }

    @EventHandler
    private void onPlayerQuit(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        event.setQuitMessage(null);
        if (contains(p)) {
            remove(p);
            event.setQuitMessage(MessageUtils.translate(RankUtils.getPrefix(RankUtils.getRank(p)) + "&e quit the game!"));
        }
    }

    @EventHandler
    private void onSpectator(PlayerSpectatorEvent e) {
        if (e.isSpectator() && !e.isCancelled()) {
            remove(e.getPlayer());

        }
    }

    @EventHandler
    private void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();
        Material mat = getMaterial(block);
        if (Main.getGameState() == Main.GameState.STARTED) {
            if (mat == Material.DIAMOND_ORE) {
                DIAMOND_ORE.giveDrop(player);
                DIAMOND_ORE.play(player, block.getLocation());
            } else if (mat == Material.SAND && block.getData() == 1) {
                RED_SAND.giveDrop(player);
                RED_SAND.play(player, block.getLocation());
            } else if (mat == Material.IRON_ORE) {
                IRON_ORE.giveDrop(player);
                IRON_ORE.play(player, block.getLocation());
            }
        } else {
            event.setCancelled(true);
        }
    }

    @EventHandler
    private void onGameFinish(GameStateChangeEvent event) {
        if (event.getNewGameState() == Main.GameState.FINISHED) {
            if (players.size() == 0) {
                Player player = players.get(0);
            } else {
                for (Player player : players) {
                    if (!Spectator.isSpectator(player)) {
                        ;
                    }
                }
            }
        }
    }

    @EventHandler
    private void onPlayerEntityInteract(PlayerInteractEntityEvent event) {
        Player p = event.getPlayer();
        Entity en = event.getRightClicked();
        if (en instanceof TNTPrimed && event.getHand() == EquipmentSlot.HAND) {
            if (p.getInventory().getItemInMainHand().equals(ItemUtils.getShears())) {
                en.remove();
            }
        }
    }

    @EventHandler
    private void onBlockPlace(BlockPlaceEvent e) {
        Block b = e.getBlock();
        Player p = e.getPlayer();
        if (b.getType() == Material.TNT) {
            b.setType(Material.AIR);
            TNTPrimed tnt = p.getWorld().spawn(b.getLocation(), TNTPrimed.class);
            tnt.setIsIncendiary(true);
            tnt.setMetadata("tnt", new FixedMetadataValue(main, p));
        }
    }

    @EventHandler // This shouldn't happen, maybe patch with custom NMS
    private void onPlayerDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        Location l = e.getEntity().getLocation();
        for (double x = l.getX(); x < 2D; x = (x + 0.1D)) {
            for (double z = l.getZ(); z < 2D; z = (z + 0.1D)) {
                p.getWorld().playEffect(new Location(p.getWorld(), x, l.getY(), z), Effect.STEP_SOUND, 152);
            }
        }
    }

    @EventHandler
    private void onPlayerDamage(EntityDamageEvent e) {
        if (Main.getGameState() == Main.GameState.STARTED) {
            if (e.getEntity() instanceof Player) {
                Player p = (Player) e.getEntity();
                p.playSound(p.getLocation(), Sound.BLOCK_STONE_BREAK, 10, 2);
				p.getWorld().playEffect(p.getLocation(), Effect.STEP_SOUND, 152);
            }
        } else {
            e.setCancelled(true);
        }
    }

    private Material getMaterial(Block block) {
        return block.getType();
    }

    private void add(Player p) {
        players.add(p);
    }

    private void remove(Player p) {
        players.remove(p);
    }

    private boolean contains(Player p) {
        return players.contains(p);
    }

    private boolean isFull() {
        return players.size() == 3;
    }
}
