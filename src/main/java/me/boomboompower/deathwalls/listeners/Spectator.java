/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.listeners;

import me.boomboompower.deathwalls.Main;
import me.boomboompower.deathwalls.events.PlayerSpectatorEvent;
import me.boomboompower.deathwalls.utils.ItemUtils;
import me.boomboompower.deathwalls.utils.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Random;

public class Spectator implements Listener {

    private static ArrayList<Player> spectators = new ArrayList<Player>();
    private Main main;

    public Spectator(Main main) {
        this.main = main;
        Bukkit.getPluginManager().registerEvents(this, main);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(main, new Runnable() {
            @Override
            public void run() {
                for(Player p : Bukkit.getOnlinePlayers()) {
                    if (p.getHealth() >= 0.0D || p.isDead()) {
                        PlayerSpectatorEvent event = new PlayerSpectatorEvent(p, true);
                        Bukkit.getPluginManager().callEvent(event);
                        if (!event.isCancelled()) {
                            spawnHead(p);
                        }
                    }
                }
            }
        }, 0, 1);
    }

    @EventHandler
    private void onGameModeChange(PlayerGameModeChangeEvent event) {
        final Player p = event.getPlayer();
        if (event.getNewGameMode() == GameMode.ADVENTURE) {
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                @Override
                public void run() {
                    p.setAllowFlight(true);
                    p.setFlying(true);
                    p.teleport(p.getLocation().add(0, 5, 0));
                }
            }, 2L);
            for(Player all : Bukkit.getOnlinePlayers()) {
                all.hidePlayer(p);
            }
            MessageUtils.sendToPlayer(p, "&cYou are now a spectator!");
        }
    }

    @EventHandler
    private void onEntityDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            if (isSpectator((Player) event.getEntity())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void onPlayerChat(AsyncPlayerChatEvent event) {
        Player p = event.getPlayer();
        if (isSpectator(p)) {
            event.setFormat(MessageUtils.translate("&7[SPECTATOR] " + p.getName() + ": " + event.getMessage()));
            for (Player player : event.getRecipients()) {
                if (!isSpectator(player)) {
                    event.getRecipients().remove(player);
                }
            }
        }
    }

    @EventHandler
    private void onPlayerInteract(PlayerInteractEvent event) {
        Player p = event.getPlayer();
        int i = new Random().nextInt(Bukkit.getOnlinePlayers().size());
        if (p.getGameMode() == GameMode.ADVENTURE || isSpectator(p)) {
            if (event.getItem().equals(ItemUtils.getSelector())) {
                Player player = (Player) Bukkit.getOnlinePlayers().toArray()[i];
                if (player != p) {
                    p.teleport(player);
                    MessageUtils.sendToPlayer(p, "&aTeleported to &2" + player.getName() + "&a!");
                }
            } else if (event.getItem().equals(ItemUtils.getQuit())) {
                p.kickPlayer("&aSent to the lobby!");
            }
        }
    }

    @EventHandler
    private void onSpectatorEvent(PlayerSpectatorEvent event) {
        Player player = event.getPlayer();
        if (!event.isCancelled()) {
            if (event.isSpectator()) {
                addSpectator(player);
                player.setGameMode(GameMode.ADVENTURE);
            }
        }
    }

    private static void spawnHead(Player player) {
        ItemStack stack = new ItemStack(Material.SKULL, 1, (short) 3);
        SkullMeta meta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
        meta.setOwner(player.getName());
        stack.setItemMeta(meta);
    }

    public static void addSpectator(Player player) {
        if (!spectators.contains(player)) spectators.add(player);
    }

    public static boolean isSpectator(Player player) {
        return spectators.contains(player);
    }
}
