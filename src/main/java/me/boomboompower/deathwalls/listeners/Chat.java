/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.listeners;

import me.boomboompower.api.old.reflection.Actionbar;
import me.boomboompower.deathwalls.Main;
import me.boomboompower.deathwalls.utils.FilterList;
import me.boomboompower.deathwalls.utils.MessageUtils;
import me.boomboompower.deathwalls.utils.RankUtils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.ArrayList;
import java.util.HashMap;

public class Chat implements Listener {

    private static ArrayList<Player> muted = new ArrayList<Player>();
    private static HashMap<Player, String> nick = new HashMap<Player, String>();
    private Main main;

    public Chat(Main main) {
        this.main = main;
        Bukkit.getPluginManager().registerEvents(this, main);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(main, new Runnable() {
            @Override
            public void run() {
                for (Player nicked : nick.keySet()) {
                    Actionbar.sendActionbar(nicked, "&fYou are currently &c&lNICKED&f!");
                }
            }
        }, 0, 10);
    }

    @EventHandler
    private void onPlayerChat(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();
        boolean toSend = true;
        e.setFormat(RankUtils.getPrefix(RankUtils.getRank(p)) + p.getName() + ": " + e.getMessage());
        if (nick.containsKey(p)) e.setFormat(RankUtils.getPrefix(RankUtils.Rank.VIP) + nick.get(p) + ": " + e.getMessage());
        for (char c : e.getMessage().toCharArray()) {
            if (!Character.isLetterOrDigit(c)) {
                toSend = false;
            } else if (FilterList.hasWords(e.getMessage())) {
                toSend = false;
            } else if (isMuted(p)) {
                MessageUtils.sendToPlayer(p, "&cYou are currently muted and cannot speak!");
                toSend = false;
            }
        }
        if (!toSend) e.setCancelled(true);
    }

    public static void setNick(Player player, String nickname) {
        nick.put(player, nickname);
        player.setPlayerListName(nickname);
    }

    public static void setMuted(Player p, boolean b) { //TODO Cleanup?
        if (b) muted.add(p);
        else muted.remove(p);
    }

    public static boolean isMuted(Player player) {
        return muted.contains(player);
    }
}