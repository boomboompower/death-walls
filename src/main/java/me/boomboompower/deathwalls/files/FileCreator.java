/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.files;

import me.boomboompower.deathwalls.Main;
import me.boomboompower.deathwalls.utils.MessageUtils;

import org.apache.commons.codec.Charsets;
import org.bukkit.configuration.file.FileConfiguration;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileCreator {

    // Code originally made with love by NickTheDev
    // https://www.spigotmc.org/threads/174917/

    private final Main PLUGIN = Main.getInstance();

    private final String CONFIG_PATH;
    private final File CONFIG_FILE;

    private FileConfiguration configurationFile;

    public FileCreator(String fileName) {
        this(fileName, ".txt", true);
    }

    public FileCreator(String fileName, String extension, boolean replace) {
        if (!extension.contains(".")) extension = "." + extension;
        CONFIG_PATH = fileName + extension;
        CONFIG_FILE = new File(PLUGIN.getDataFolder(), CONFIG_PATH);

        create(replace);
    }

    private void create(boolean replace) {
        if (!CONFIG_FILE.exists()) PLUGIN.saveResource(CONFIG_PATH, replace);

        configurationFile = YamlConfiguration.loadConfiguration(CONFIG_FILE);
    }

    public void save() {
        try {
            configurationFile.save(CONFIG_FILE);
        } catch (Exception exception) {
            MessageUtils.sendToConsole("&cFailed to save file &4" + CONFIG_PATH);
        }
    }

    public void reload() {
        configurationFile = YamlConfiguration.loadConfiguration(CONFIG_FILE);

        InputStream configStream = PLUGIN.getResource(CONFIG_PATH);

        if (configStream != null) {
            configurationFile.setDefaults(YamlConfiguration.loadConfiguration(new InputStreamReader(configStream, Charsets.UTF_8)));
        } else {
            MessageUtils.sendToConsole("&cFailed to reload file &4" + CONFIG_PATH);
        }
    }

    public FileConfiguration getConfig() {
        return configurationFile;
    }
}
