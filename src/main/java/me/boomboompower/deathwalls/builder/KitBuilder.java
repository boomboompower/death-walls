/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.builder;

import com.google.common.collect.Maps;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class KitBuilder {

    private static ItemStack air = new ItemStack(Material.AIR);

    private static ItemStack helmet;
    private static ItemStack chestplate;
    private static ItemStack leggings;
    private static ItemStack boots;

    private static ItemStack bow;
    private static ItemStack arrow;

    private static ItemStack weapon;
    private static ItemStack food;

    private static HashMap<Integer, ItemStack> other = Maps.newHashMap();

    public KitBuilder(ItemStack helm, ItemStack chest, ItemStack legs, ItemStack feet) {
        this(helm, chest, legs, feet, null, null, null, null);
    }
    
    public KitBuilder(ItemStack helm, ItemStack chest, ItemStack legs, ItemStack feet, ItemStack wep) {
        this(helm, chest, legs, feet, wep, null, null, null);
    }

    public KitBuilder(ItemStack helm, ItemStack chest, ItemStack legs, ItemStack feet, ItemStack wep, ItemStack bow, ItemStack arrow) {
        this(helm, chest, legs, feet, wep, bow, arrow, null);
    }

    public KitBuilder(ItemStack helm, ItemStack chest, ItemStack legs, ItemStack feet, ItemStack wep, ItemStack bow_, ItemStack arrow_, ItemStack meal) {
        helmet = helm != null ? helm : air;
        chestplate = chest != null ? chest : air;
        leggings = legs != null ? legs : air;
        boots = feet != null ? feet : air;
        bow = bow_ != null ? bow_ : air;
        arrow = arrow_!= null ? arrow_: air;
        weapon = wep != null ? wep : air;
        food = meal != null ? meal : air;
    }

    public ItemStack getHelmet() {
        return helmet != null ? helmet : air;
    }

    public ItemStack getChestplate() {
        return chestplate != null ? chestplate : air;
    }

    public ItemStack getLeggings() {
        return leggings != null ? leggings : air;
    }

    public ItemStack getBoots() {
        return boots != null ? boots : air;
    }

    public ItemStack getWeapon() {
        return weapon != null ? weapon : air;
    }

    public ItemStack getBow() {
        return bow != null ? bow : air;
    }

    public ItemStack getArrow() {
        return arrow != null ? arrow : air;
    }

    public ItemStack getFood() {
        return food != null ? food : air;
    }

    public ItemStack getExtra(int slot) {
        return other.get(slot) != null ? other.get(slot) : air;
    }

    public void addExtra(int slot, ItemStack item) {
        other.put(slot, item);
    }

    public void redo(Slot slot, ItemStack i) {
        switch (slot) {
            case HEAD:
                helmet = i;
                break;
            case CHEST:
                chestplate = i;
                break;
            case LEGS:
                leggings = i;
                break;
            case FEET:
                boots = i;
                break;
            case WEAPON:
                weapon = i;
                break;
            case BOW:
                bow = i;
                break;
            case ARROW:
                arrow = i;
                break;
            case FOOD:
                food = i;
                break;
            case EXTRA:
                if (!other.containsValue(i)) other.put(other.size(), i);
                break;
        }
    }

    public void toPlayer(Player p) {
        p.getInventory().setHelmet(getHelmet());
        p.getInventory().setChestplate(getChestplate());
        p.getInventory().setLeggings(getLeggings());
        p.getInventory().setBoots(getBoots());
        p.getInventory().setItem(0, getWeapon());
        p.getInventory().setItem(1, getFood());
        p.getInventory().setItem(8, getBow());
        p.getInventory().setItem(35, getArrow());
        for (int i = 0; i > other.size(); i++) {
            p.getInventory().addItem(other.get(i));
        }
    }

    public enum Slot {
        HEAD,
        CHEST,
        LEGS,
        FEET,
        WEAPON,
        BOW,
        ARROW,
        FOOD,
        EXTRA;

        Slot() {}
    }

    @Override
    public String toString() {
        return "An API used to get and apply a kit to a player";
    }
}
