/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls;

import me.boomboompower.deathwalls.commands.CommandRegister;
import me.boomboompower.deathwalls.events.GameStateChangeEvent;
import me.boomboompower.deathwalls.listeners.ListenerRegister;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class Main extends JavaPlugin {

    private static Main instance;
    private static GameState gameState;

    private Main() {}

    @Override
    public void onEnable() {
        new ListenerRegister(this);
        new CommandRegister(this);
        instance = this;
        gameState = GameState.WAITING;

        try  {
            for (World world : Bukkit.getWorlds()) {
                world.setGameRuleValue("doTileDrops", "false");
            }
        } catch (Exception ex) {}
    }

    public static Main getInstance() {
        return instance;
    }

    public static void setGameState(GameState newState) {
        GameStateChangeEvent event = new GameStateChangeEvent(gameState, newState);
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isCancelled()) {
            gameState = event.getNewGameState();
        }
    }

    public static GameState getGameState() {
        return gameState;
    }

    public enum GameState {
        WAITING,
        STARTED,
        FINISHED;

        GameState() {}
    }
}
