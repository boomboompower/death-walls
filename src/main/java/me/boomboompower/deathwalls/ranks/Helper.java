/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.ranks;

import me.boomboompower.deathwalls.utils.RankUtils;

import org.bukkit.entity.Player;

public abstract class Helper implements Rank {

    private Helper() {}

    /**
     * Checks if a player is a helper.
     *
     * @param player Player to check for the helper rank
     * @return true if the player is a helper
     */
    public boolean hasRank(Player player) {
        return RankUtils.getRank(player) == RankUtils.Rank.HELPER;
    }

    /**
     * Gets the rank from the class
     *
     * @return the rank
     */
    @Override
    public RankUtils.Rank getRank() {
        return RankUtils.Rank.HELPER;
    }

    /**
     * Gets a list of the ranks permissions
     *
     * @return List of helper permissions
     */
    public String[] getPermissionList() {
        return new String[] {
                "deathwalls.build",
                "deathwalls.chat",
        };
    }
}
