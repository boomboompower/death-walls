/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.ranks;

import me.boomboompower.deathwalls.utils.RankUtils;

import org.bukkit.entity.Player;

public abstract class Admin implements Rank {

    private Admin() {}

    /**
     * Checks if a player is a admin.
     *
     * @param player Player to check for admin rank
     * @return true if the player is an admin
     */
    public boolean hasRank(Player player) {
        return RankUtils.getRank(player) == RankUtils.Rank.ADMINISTRATOR;
    }

    /**
     * Gets the rank from the class
     *
     * @return the rank
     */
    @Override
    public RankUtils.Rank getRank() {
        return RankUtils.Rank.ADMINISTRATOR;
    }

    /**
     * Gets a list of the ranks permissions
     *
     * @return List of admin permissions
     */
    public String[] getPermissionList() {
        return new String[] {
                "deathwalls.build",
                "deathwalls.chat",
                "deathwalls.youtube.nick",
        };
    }
}
