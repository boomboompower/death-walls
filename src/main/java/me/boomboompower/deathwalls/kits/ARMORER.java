/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.kits;

import me.boomboompower.api.builder.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class ARMORER implements Builder {

    public ARMORER() {}

    @Override
    public ItemStack getHelmet() {
        ItemBuilder builder = new ItemBuilder(Material.CHAINMAIL_HELMET);
        builder.setDisplayName("&aArmorer helmet");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getChestplate() {
        ItemBuilder builder = new ItemBuilder(Material.CHAINMAIL_CHESTPLATE);
        builder.setDisplayName("&aArmorer chestplate");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getLeggings() {
        ItemBuilder builder = new ItemBuilder(Material.CHAINMAIL_LEGGINGS);
        builder.setDisplayName("&aArmorer leggings");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getBoots() {
        ItemBuilder builder = new ItemBuilder(Material.IRON_BOOTS);
        builder.addEnchantment(Enchantment.PROTECTION_FALL, 2);
        builder.setDisplayName("&aArmorer boots");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getWeapon() {
        ItemBuilder builder = new ItemBuilder(Material.WOOD_SWORD);
        builder.addEnchantment(Enchantment.DAMAGE_ALL, 1);
        builder.setDisplayName("&aArmorer sword");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getFood() {
        ItemBuilder builder = new ItemBuilder(Material.BAKED_POTATO);
        builder.setDisplayName("&aBaked potato");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getBow() {
        return new ItemStack(Material.AIR);
    }

    @Override
    public ItemStack getArrow() {
        return new ItemStack(Material.AIR);
    }
}
