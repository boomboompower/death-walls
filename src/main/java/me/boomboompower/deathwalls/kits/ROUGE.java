/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.kits;

import me.boomboompower.api.builder.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class ROUGE implements Builder {

    private ROUGE() {}

    @Override
    public ItemStack getHelmet() {
        ItemBuilder builder = new ItemBuilder(Material.CHAINMAIL_HELMET);
        builder.setDisplayName("&eRouge helmet");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getChestplate() {
        ItemBuilder builder = new ItemBuilder(Material.GOLD_CHESTPLATE);
        builder.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 2);
        builder.setDisplayName("&eRouge chestplate");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getLeggings() {
        ItemBuilder builder = new ItemBuilder(Material.CHAINMAIL_LEGGINGS);
        builder.setDisplayName("&eRouge leggings");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getBoots() {
        ItemBuilder builder = new ItemBuilder(Material.GOLD_BOOTS);
        builder.addEnchantment(Enchantment.PROTECTION_FIRE, 2);
        builder.setDisplayName("&eRouge boots");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getWeapon() {
        ItemBuilder builder = new ItemBuilder(Material.WOOD_SWORD);
        builder.addEnchantment(Enchantment.KNOCKBACK, 4);
        builder.setDisplayName("&eRouge sword");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getFood() {
        ItemBuilder builder = new ItemBuilder(Material.CARROT);
        builder.setDisplayName("&eCarrot");
        builder.setAmount(7);
        return builder.getItemStack();
    }

    @Override
    public ItemStack getBow() {
        ItemBuilder builder = new ItemBuilder(Material.BOW);
        builder.addEnchantment(Enchantment.ARROW_KNOCKBACK, 1);
        builder.setDisplayName("&eRouge bow");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getArrow() {
        return new ItemStack(Material.AIR);
    }
}
