/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.kits;

import me.boomboompower.api.builder.ItemBuilder;
import me.boomboompower.deathwalls.builder.KitBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ARCHER implements Builder {

    private ARCHER() {}

    @Override
    public ItemStack getHelmet() {
        ItemBuilder builder = new ItemBuilder(Material.IRON_HELMET);
        builder.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        builder.setDisplayName("&bArchers armor");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getChestplate() {
        ItemBuilder builder = new ItemBuilder(Material.GOLD_BOOTS);
        builder.setDisplayName("&bArchers armor");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getLeggings() {
        ItemBuilder builder = new ItemBuilder(Material.GOLD_BOOTS);
        builder.setDisplayName("&bArchers armor");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getBoots() {
        ItemBuilder builder = new ItemBuilder(Material.GOLD_BOOTS);
        builder.setDisplayName("&bArchers armor");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getWeapon() {
        ItemBuilder builder = new ItemBuilder(Material.STONE_SWORD);
        builder.setDisplayName("&bArchers armor");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getFood() {
        ItemBuilder builder = new ItemBuilder(Material.COOKED_RABBIT);
        builder.setDisplayName("&bCooked rabbit");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getBow() {
        ItemBuilder builder = new ItemBuilder(Material.BOW);
        builder.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
        builder.setDisplayName("&bArcher bow");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getArrow() {
        ItemBuilder builder = new ItemBuilder(Material.ARROW);
        builder.setDisplayName("&bSharp arrow");
        return builder.getItemStack();
    }

    public void toPlayer(Player player) {
        new KitBuilder(getHelmet(), getChestplate(), getLeggings(), getBoots(), getWeapon(), getBow(), getArrow(), getFood());
    }
}
