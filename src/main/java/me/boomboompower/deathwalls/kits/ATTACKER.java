/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.kits;

import me.boomboompower.api.builder.ItemBuilder;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ATTACKER implements Builder {

    private ATTACKER() {}

    @Override
    public ItemStack getHelmet() {
        ItemBuilder builder = new ItemBuilder(Material.GOLD_HELMET);
        builder.setDisplayName("&aKnights helmet");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getChestplate() {
        ItemBuilder builder = new ItemBuilder(Material.GOLD_CHESTPLATE);
        builder.setDisplayName("&cKnights chestplate");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getLeggings() {
        ItemBuilder builder = new ItemBuilder(Material.GOLD_LEGGINGS);
        builder.setDisplayName("&cKnights leggings");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getBoots() {
        ItemBuilder builder = new ItemBuilder(Material.GOLD_BOOTS);
        builder.setDisplayName("&cKnights boots");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getWeapon() {
        ItemBuilder builder = new ItemBuilder(Material.DIAMOND_SWORD);
        builder.setDisplayName("&cKnights sword");
        return builder.getItemStack();
    }

    @Override
    public ItemStack getFood() {
        ItemBuilder builder = new ItemBuilder(Material.COOKED_CHICKEN);
        builder.setDisplayName("&cCooked Chicken");
        builder.setAmount(4);
        return builder.getItemStack();
    }

    @Override
    public ItemStack getBow() {
        return new ItemStack(Material.AIR);
    }

    @Override
    public ItemStack getArrow() {
        return new ItemStack(Material.AIR);
    }
}
