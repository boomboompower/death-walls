/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.blocks;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Random;

public class IRON_ORE { // TODO

    private IRON_ORE() {}

    public static HashMap<Integer, ItemStack> getDrops() {
        HashMap<Integer, ItemStack> drops = new HashMap<Integer, ItemStack>();

        for (int i = 0; i < drops.size(); i++) {
            if (!drops.containsKey(i)) drops.put(i, getNothing());
        }
        return drops;
    }

    private static ItemStack getNothing() {
        return new ItemStack(Material.AIR);
    }

    public static void giveDrop(Player p) {
        p.getInventory().addItem(getDrops().get(new Random().nextInt(50)));
    }

    public static void play(Player p, Location loc) {
        p.getWorld().spigot().playEffect(loc, Effect.CRIT);
        p.playSound(loc, Sound.UI_BUTTON_CLICK, 10, 2);
    }
}
