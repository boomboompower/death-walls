/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.deathwalls.blocks;

import me.boomboompower.api.builder.ItemBuilder;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Random;

public class RED_SAND {

    private RED_SAND() {}

    public static HashMap<Integer, ItemStack> getDrops() {
        HashMap<Integer, ItemStack> drops = new HashMap<Integer, ItemStack>();
        drops.put(10, getTNT());
        drops.put(35, getArrow());
        drops.put(50, getBow());
        for (int i = 0; i < drops.size(); i++) {
            if (!drops.containsKey(i)) drops.put(i, getNothing());
        }
        return drops;
    }

    private static ItemStack getTNT() {
        return new ItemStack(Material.TNT, 3);
    }

    private static ItemStack getWeapon() {
        ItemBuilder builder = new ItemBuilder(Material.STONE_SWORD);
        builder.setDisplayName("&cShattered sword");
        builder.setDurability(136);
        return builder.getItemStack();
    }

    private static ItemStack getBow() {
        ItemBuilder builder = new ItemBuilder(Material.BOW);
        builder.setDisplayName("&cShattered bow");
        builder.setDurability(192);
        return builder.getItemStack();
    }

    private static ItemStack getArrow() {
        return new ItemStack(Material.ARROW, 5);
    }

    private static ItemStack getNothing() {
        return new ItemStack(Material.AIR);
    }

    public static void giveDrop(Player p) {
        p.getInventory().addItem(getDrops().get(new Random().nextInt(50)));
    }

    public static void play(Player p, Location loc) {
        p.getWorld().spigot().playEffect(loc, Effect.CRIT);
        p.playSound(loc, Sound.UI_BUTTON_CLICK, 10, 2);
    }
}
