/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.api.builder;

import me.boomboompower.deathwalls.utils.MessageUtils;

import org.apache.commons.lang.Validate;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.List;

public class ItemBuilder {

    private ItemStack itemStack;
    private ItemMeta itemMeta;

    public ItemBuilder(Material material) {
        this(new ItemStack(material));
    }

    public ItemBuilder(ItemStack itemStack) {
        this.itemStack = itemStack;
        this.itemMeta = itemStack.getItemMeta();
    }

    public ItemStack getItemStack() {
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public void setDurability(int durability) {
        itemStack.setDurability((short) durability);
    }

    public void setLore(String... lore) {
        Validate.notNull(lore, "Lore cannot be null!");
        List<String> l = new ArrayList<String>();
        for (String s : lore) {
            l.add(MessageUtils.translate(s));
        }
        itemMeta.setLore(l);
    }

    public void setAmount(int amount) {
        itemStack.setAmount(amount);
    }

    public void addEnchantment(Enchantment enchantment, int level) {
        itemMeta.addEnchant(enchantment, level > 0 ? level : 1, true);
    }

    public void addFlag(ItemFlag... itemFlags) {
        itemMeta.addItemFlags(itemFlags);
    }

    public void removeFlag(ItemFlag... itemFlags) {
        itemMeta.removeItemFlags(itemFlags);
    }

    public void addPotionEffect(PotionEffect potionEffect) {
        if (itemStack.getType() == Material.POTION || itemStack.getType() == Material.LINGERING_POTION || itemStack.getType() == Material.SPLASH_POTION) {
            ((PotionMeta) itemMeta).addCustomEffect(potionEffect, true);
        }
    }

    public void clearEnchantments() {
        itemStack.getEnchantments().clear();
    }

    public void setDisplayName(String name) {
        itemMeta.setDisplayName(name != null ? MessageUtils.translate(name) : "None");
    }

    public void setUnbreakable(boolean unbreakable) {
        itemMeta.spigot().setUnbreakable(unbreakable);
        if (unbreakable) addFlag(ItemFlag.HIDE_UNBREAKABLE);
    }
}
