/*
    Copyright (C) 2016 boomboompower

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package me.boomboompower.api.old.utils;

import org.bukkit.Bukkit;

import java.lang.reflect.Field;

@Deprecated // API Subject to change
public class ReflectionUtils {

    private ReflectionUtils() {}

    public static Field getValue(String fieldName, Object instance) {
        try {
            Field field = instance.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return field;
        } catch (Exception ex) {
            return null;
        }
    }

    public static void setValue(String fieldName, Object instance, Object value) {
        try {
            Field field = instance.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(instance, value);
        } catch (Exception ex) {}
    }

    public static String getVersion() {
        return Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
    }

    public static Class<?> getNMSClass(String name) {
        return OptionUtils.getClass("net.minecraft.server." + getVersion() + "." + name);
    }

    public static Class<?> getCraftClass(String name) {
        return OptionUtils.getClass("org.bukkit.craftbukkit." + getVersion() + "." + name);
    }
}
